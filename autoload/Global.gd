extends Node

const DEFAULT_SCREEN_COLOR = Color(0.3, 0.3, 0.3, 1.0)
const GREEN_SCREEN_COLOR = Color(0.0, 1.0, 0.0, 1.0)

var green_screen_on := false setget set_green_screen_on

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle_green_screen"):
		set_green_screen_on(not green_screen_on)

func set_green_screen_on(_green_screen_on: bool) -> void:
	if green_screen_on != _green_screen_on:
		green_screen_on = _green_screen_on
		if green_screen_on:
			VisualServer.set_default_clear_color(GREEN_SCREEN_COLOR)
		else:
			VisualServer.set_default_clear_color(DEFAULT_SCREEN_COLOR)
