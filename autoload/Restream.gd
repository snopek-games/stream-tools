extends Node

# This should be replaced by other code.
var client_id := 'example_client_id'
var client_secret := 'example_client_secret'

var _access_token := {}
var _chat_connection: WebSocketClient

enum AccessTokenState {
	INVALID,
	VALID,
	EXPIRED,
}

# @todo Add the rest of the event types.
enum ChatEventType {
	DiscordText = 1,
	TwitchText = 4,
	YouTubeText = 5,
	FacebookPersonalText = 11,
	FacebookPageText = 13,
	PeriscopeText = 15,
}

signal error (msg)
signal access_token_refreshed ()
signal chat_data_received (data)

func _ready() -> void:
	pass

func load_access_token(filename: String) -> void:
	var file = File.new()
	file.open(filename, File.READ)
	var content = file.get_as_text()
	file.close()

	var result := JSON.parse(content)
	if result.error == OK and result.result is Dictionary:
		_access_token = result.result
	else:
		push_error("Unable to load access token from %s" % filename)
		return
	
	if check_access_token() == AccessTokenState.EXPIRED:
		refresh_access_token(false)

func save_access_token(filename: String) -> void:
	var file = File.new()
	file.open(filename, File.WRITE)
	file.store_string(JSON.print(_access_token))
	file.close()

func _parse_date(iso_date: String) -> Dictionary:
	var date := iso_date.split("T")[0].split("-")
	var time := iso_date.split("T")[1].trim_suffix("Z").split(":")
	
	return {
		year = date[0],
		month = date[1],
		day = date[2],
		hour = time[0],
		minute = time[1],
		second = time[2],
	}

func check_access_token() -> int:
	if _access_token.size() == 0:
		return AccessTokenState.INVALID
	if not _access_token.has('access_token'):
		return AccessTokenState.INVALID
	if not _access_token.has('refresh_token'):
		return AccessTokenState.INVALID
	if not _access_token.has('expiry'):
		return AccessTokenState.INVALID
	
	if not _access_token.has('expiry_unix_time'):
		var datetime = _parse_date(_access_token['expiry'])
		_access_token['expiry_unix_time'] = OS.get_unix_time_from_datetime(datetime)
	
	if OS.get_unix_time() >= _access_token['expiry_unix_time']:
		return AccessTokenState.EXPIRED
	
	return AccessTokenState.VALID

func _query_string_from_dict(data: Dictionary) -> String:
	return HTTPClient.new().query_string_from_dict(data)

func refresh_access_token(check_token: bool = true):
	if check_token and check_access_token() == AccessTokenState.INVALID:
		return
	
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.connect("request_completed", self, "_on_http_request_refresh_access_token", [http_request])
	
	var headers := [
		'Content-Type: application/x-www-form-urlencoded',
	]
	
	var body := {
		grant_type = 'refresh_token',
		refresh_token = _access_token['refresh_token'],
		client_id = client_id,
		client_secret = client_secret,
	}
	http_request.request("https://api.restream.io/oauth/token", PoolStringArray(headers), false, HTTPClient.METHOD_POST, _query_string_from_dict(body))

func _on_http_request_refresh_access_token(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray, http_request: HTTPRequest) -> void:
	http_request.queue_free()
	
	var json_result := JSON.parse(body.get_string_from_utf8())
	if json_result.error != OK or not json_result.result is Dictionary:
		emit_signal("error", "Unable to parse JSON from server")
		return
	
	var data: Dictionary = json_result.result
	if data.has('error'):
		emit_signal('error', data['error']['message'])
		return
	
	_access_token = {
		access_token = data['access_token'],
		token_type = data['token_type'],
		refresh_token = data['refresh_token'],
		expiry = data['accessTokenExpiresAt'],
		expiry_unix_time = data['expires'],
	}
	
	emit_signal("access_token_refreshed")

func open_chat_connection() -> void:
	if _chat_connection:
		return
	
	# @todo this is terrible!
	var token_state = check_access_token() 
	if token_state == AccessTokenState.EXPIRED:
		refresh_access_token()
		emit_signal("error", "Refreshing token - try again later")
		return
	if token_state == AccessTokenState.INVALID:
		emit_signal("error", "Invalid access token")
		return
	
	_chat_connection = WebSocketClient.new()
	_chat_connection.connect("connection_closed", self, "_on_chat_connection_closed")
	_chat_connection.connect("connection_error", self, "_on_chat_connection_error")
	_chat_connection.connect("data_received", self, "_on_chat_connection_data_received")
	
	var url = "wss://chat.api.restream.io/ws?accessToken=" + _access_token['access_token']
	var error = _chat_connection.connect_to_url(url)
	if error != OK:
		emit_signal("error", "Unable to connect to chat web socket")

func close_chat_connection() -> void:
	if not _chat_connection:
		return
	
	_chat_connection.disconnect("connection_closed", self, "_on_chat_connection_closed")
	_chat_connection.disconnect("connection_error", self, "_on_chat_connection_error")
	_chat_connection.disconnect("data_received", self, "_on_chat_connection_data_received")
	
	_chat_connection.disconnect_from_host()
	_chat_connection = null

func is_chat_connection_open() -> bool:
	return _chat_connection and _chat_connection.get_peer(1).is_connected_to_host()

func _on_chat_connection_closed(was_clean: bool) -> void:
	emit_signal("error", "Chat connection closed")

func _on_chat_connection_error() -> void:
	emit_signal("error", "Chat connection error")

func _on_chat_connection_data_received() -> void:
	var data = _chat_connection.get_peer(1).get_packet().get_string_from_utf8()
	
	var json_result = JSON.parse(data)
	if json_result.error == OK and json_result.result is Dictionary:
		emit_signal("chat_data_received", json_result.result)
	else:
		emit_signal("error", "Received invalid JSON data from chat connection")

func _process(delta: float) -> void:
	if _chat_connection:
		_chat_connection.poll()

