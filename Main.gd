extends Node2D

onready var background = $Layers/Background
onready var countdown = $Layers/Countdown
onready var music = $Layers/Music
onready var billboard = $Layers/Billboard
onready var chat = $Layers/Chat

func _ready() -> void:
	randomize()

	# Always spawn me, yo!
	var me = chat.spawn_character('dsnopek')
	me.fade_out_on_speech = false

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("reset_all"):
		background.visible = true
		countdown.visible = true
		countdown.reset()
		music.visible = false
		music.stop()
		billboard.visible = false
		chat.visible = true
		Global.green_screen_on = false
		get_tree().set_input_as_handled()

	if event.is_action_pressed("switch_to_stream_mode"):
		background.visible = false
		countdown.visible = false
		countdown.reset()
		music.visible = false
		music.stop()
		billboard.visible = false
		chat.visible = true
		Global.green_screen_on = true
		get_tree().set_input_as_handled()

	if event.is_action_pressed("reconnect_to_restream"):
		chat.reconnect_to_restream()
		get_tree().set_input_as_handled()

	if event.is_action_pressed("toggle_background"):
		background.visible = not background.visible
		get_tree().set_input_as_handled()

	if event.is_action_pressed("toggle_countdown"):
		countdown.visible = not countdown.visible
		get_tree().set_input_as_handled()

	if countdown.visible and event.is_action_pressed("reset_countdown"):
		countdown.reset()
		get_tree().set_input_as_handled()

	if event.is_action_pressed("toggle_billboard"):
		billboard.visible = not billboard.visible
		if billboard.visible:
			billboard.reset()
		get_tree().set_input_as_handled()

	if event.is_action_pressed("toggle_music"):
		music.visible = not music.visible
		if music.visible:
			music.play()
		else:
			music.stop()
		get_tree().set_input_as_handled()

	if event.is_action_pressed("toggle_chat"):
		chat.visible = not chat.visible
		get_tree().set_input_as_handled()
