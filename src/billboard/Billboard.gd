extends Node2D

onready var label = $Control/Label
onready var color_rect = $Control/ColorRect
onready var timer = $Timer

var messages = [
	"Say something in chat to spawn a character!",
	"Only Discord chat appears in the speech bubbles",
	"Join the Discord at SnopekGames.com/Discord",
]
var index := 0

func _ready() -> void:
	reset()

func reset() -> void:
	index = 0
	label.visible = true
	color_rect.visible = true
	label.text = messages[index]
	timer.start()

func _on_Timer_timeout() -> void:
	if label.visible:
		label.visible = false
	else:
		index += 1
		if index >= messages.size():
			index = 0
		label.text = messages[index]
		label.visible = true
	color_rect.visible = label.visible
