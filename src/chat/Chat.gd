extends Node2D

const Character = preload("res://src/characters/Character.tscn")

const RESTREAM_TOKEN_FILENAME = 'user://token.json'

onready var chat_text_box = $Control/ChatTextBox

var characters := {}

func _ready() -> void:
	Restream.client_id = OS.get_environment('RESTREAM_CLIENT_ID')
	Restream.client_secret = OS.get_environment('RESTREAM_CLIENT_SECRET')

	Restream.connect("error", self, "_on_Restream_error")
	Restream.connect("access_token_refreshed", self, "_on_Restream_access_token_refreshed")
	Restream.connect("chat_data_received", self, "_on_Restream_chat_data_received")

	Restream.load_access_token(RESTREAM_TOKEN_FILENAME)
	reconnect_to_restream()

func reconnect_to_restream() -> void:
	Restream.close_chat_connection()
	if Restream.check_access_token() == Restream.AccessTokenState.VALID:
		Restream.open_chat_connection()

func _on_Restream_error(msg: String) -> void:
	print ("ERROR: %s" % msg)

	if msg == 'Chat connection closed':
		reconnect_to_restream()

func _on_Restream_access_token_refreshed() -> void:
	print ("access token refreshed")
	Restream.save_access_token(RESTREAM_TOKEN_FILENAME)
	if not Restream.is_chat_connection_open():
		Restream.open_chat_connection()

func spawn_character(chat_name: String):
	if not characters.has(chat_name):
		var character = Character.instance()
		add_child(character)
		character.set_name_label(chat_name)
		character.connect("character_clicked", self, "_on_character_clicked", [character])
		character.connect("character_faded_out", self, "_on_character_faded_out", [character])

		characters[chat_name] = character
	else:
		var character = characters[chat_name]
		if character.get_parent() == null:
			add_child(character)
			character.randomize_position_and_vector()

	return characters[chat_name]

func _on_character_clicked(event: InputEventMouseButton, character):
	if event.button_index == BUTTON_RIGHT:
		remove_child(character)
		character.queue_free()

func _on_character_faded_out(character) -> void:
	var parent = character.get_parent()
	if parent:
		parent.remove_child(character)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("reset_characters"):
		for character in characters.values():
			character.randomize_position_and_vector()
	#elif event.is_action_pressed("debug_speech"):
	#	if characters.size() > 0:
	#		characters.values()[0].add_speech("DEBUG")

func _has_discord_role(author: Dictionary, role_name: String) -> bool:
	for role in author['roles']:
		if role['name'] == role_name:
			return true
	return false

func _on_Restream_chat_data_received(data: Dictionary) -> void:
	print (data)

	if data['action'] == 'event':
		var payload: Dictionary = data['payload']
		var event_type_id: int = payload['eventTypeId']
		var event_payload: Dictionary = payload['eventPayload']
		if not event_type_id in [1, 4, 5, 11, 13, 15]:
			return
		if event_payload.get('bot', false):
			return

		match event_type_id:
			Restream.ChatEventType.DiscordText:
				chat_text_box.add_chat_text(event_payload['author']['name'], event_payload['text'])
				var character = spawn_character(event_payload['author']['name'])
				if _has_discord_role(event_payload['author'], 'trusted'):
					character.add_speech(event_payload['text'])
				else:
					character.add_speech("$?@#!")
			Restream.ChatEventType.TwitchText, \
			Restream.ChatEventType.YouTubeText, \
			Restream.ChatEventType.PeriscopeText:
				chat_text_box.add_chat_text(event_payload['author']['displayName'], event_payload['text'])
				var character = spawn_character(event_payload['author']['displayName'])
				character.add_speech("$?@#!")
			Restream.ChatEventType.FacebookPersonalText, \
			Restream.ChatEventType.FacebookPageText:
				chat_text_box.add_chat_text(event_payload['author'].get('name', 'Facebook User'), event_payload['text'])
				var character = spawn_character(event_payload['author'].get('name', 'Facebook User'))
				character.add_speech("$?@#!")
	elif data['action'] == 'reply_created':
		var payload: Dictionary = data['payload']
		chat_text_box.add_chat_text('dsnopek', payload['text'])
		var character = spawn_character('dsnopek')
		character.add_speech(payload['text'])
