extends Node2D

const Character = preload("res://src/characters/Character.tscn")

func _ready() -> void:
	randomize()
	for i in range(10):
		var character = Character.instance()
		add_child(character)

