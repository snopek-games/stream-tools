extends Node2D

onready var form = $Control/Form
onready var counter = $Control/Counter
onready var countdown_field = $Control/Form/CountdownField
onready var countdown_label = $Control/Counter/Label
onready var timer = $Timer

var start_time := 0 setget _set_readonly_value
var end_time := 0 setget _set_readonly_value

func _set_readonly_value(_value) -> void:
	pass

func _ready() -> void:
	countdown_field.grab_focus()

func reset() -> void:
	timer.stop()
	form.visible = true
	counter.visible = false
	countdown_field.grab_focus()

func _on_StartButton_pressed() -> void:
	var text_time: String = countdown_field.text
	var text_parts: Array = text_time.split(':')
	if text_parts.size() != 2:
		OS.alert("Invalid time")
		return
	var seconds: int = (int(text_parts[0]) * 60) + int(text_parts[1])
	
	start_time = OS.get_system_time_secs()
	end_time = start_time + seconds

	timer.start()
	form.visible = false
	counter.visible = true
	
	_on_Timer_timeout()

func _on_Timer_timeout() -> void:
	var total_seconds: int = end_time - OS.get_system_time_secs()
	var negative: bool = total_seconds < 0
	total_seconds = int(abs(total_seconds))
	var minutes = total_seconds / 60
	var seconds = total_seconds % 60
	
	countdown_label.text = "Stream starting in "
	if negative:
		countdown_label.text += "-"
	countdown_label.text += str(minutes) + ":" + str(seconds).pad_zeros(2)

func _on_CountdownField_text_entered(new_text: String) -> void:
	_on_StartButton_pressed()

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel") and countdown_field.has_focus():
		countdown_field.release_focus()
		get_tree().set_input_as_handled()
