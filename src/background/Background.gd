extends Node2D

onready var color_rect = $ColorRect

func _process(delta: float) -> void:
	color_rect.material.set_shader_param("seed", Vector2(sin(OS.get_ticks_usec()), sin(OS.get_ticks_usec())) * 0.1)
	
