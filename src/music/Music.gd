extends Node2D

onready var audio_stream_player = $AudioStreamPlayer

func play() -> void:
	audio_stream_player.play()

func stop() -> void:
	audio_stream_player.stop()
