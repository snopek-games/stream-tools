extends Node2D

onready var character = $Character
onready var line_edit = $LineEdit

func _on_LineEdit_text_entered(new_text: String) -> void:
	character.set_speech(new_text)
	line_edit.text = ''
