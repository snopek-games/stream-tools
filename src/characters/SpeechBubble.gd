extends Node2D

onready var container = $PanelContainer
onready var label = $PanelContainer/Label
onready var pointer_texture = $PointerTexture

const MAX_WIDTH := 150

func _reset() -> void:
	label.text = ''
	label.autowrap = false
	label.rect_min_size = Vector2.ZERO
	label.rect_size = Vector2.ZERO
	container.rect_size = Vector2.ZERO

func set_speech_text(text: String) -> void:
	_reset()
	label.text = text
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	if label.rect_size.x >= MAX_WIDTH:
		label.autowrap = true
		label.rect_min_size.x = MAX_WIDTH
	container.rect_size = Vector2.ZERO
	yield(get_tree(), "idle_frame")
	#yield(get_tree(), "idle_frame")
	container.rect_position.y = -container.rect_size.y

func _on_LineEdit_text_entered(new_text: String) -> void:
	set_speech_text(new_text)
