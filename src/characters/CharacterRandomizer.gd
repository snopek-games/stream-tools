extends Node2D

onready var character_sprite = $CharacterSprite

func _ready() -> void:
	randomize()
	character_sprite.randomize_character()

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventKey and event.is_pressed():
		character_sprite.randomize_character()
