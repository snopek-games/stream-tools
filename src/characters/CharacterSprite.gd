extends Node2D

onready var body = $Body
onready var upper = $Upper
onready var lower = $Lower
onready var hair = $Hair
onready var hat = $Hat
onready var shield = $Shield
onready var weapon = $Weapon

func _randomize_frame_any(sprite: Sprite) -> void:
	sprite.frame = randi() % (sprite.hframes * sprite.vframes)

func _randomize_frame_including_off(sprite: Sprite) -> void:
	var frame = randi() % ((sprite.hframes * sprite.vframes) + 1)
	if frame == 0:
		sprite.visible = false
	else:
		sprite.visible = true
		sprite.frame = frame - 1

func _randomize_frame_off_chance(sprite: Sprite, off_chance: float) -> void:
	var on_val = true
	var off_val = false
	if off_chance > 0.5:
		off_chance = 1.0 - off_chance
		on_val = false
		off_val = true
	
	var total := int(1.0 / off_chance)
	var off_count := int(ceil(total * off_chance))
	var list := []
	
	for i in range(total):
		if i < off_count:
			list.append(off_val)
		else:
			list.append(on_val)
	
	#print (off_chance)
	#print (list)
	
	if list[randi() % list.size()]:
		sprite.visible = true
		_randomize_frame_any(sprite)
	else:
		sprite.visible = false
	

func randomize_character() -> void:
	_randomize_frame_any(body)
	_randomize_frame_including_off(upper)
	_randomize_frame_including_off(lower)
	_randomize_frame_including_off(hair)
	_randomize_frame_off_chance(hat, 0.75)
	_randomize_frame_off_chance(shield, 0.90)
	_randomize_frame_off_chance(weapon, 0.90)

func get_size() -> Vector2:
	return body.get_rect().size
