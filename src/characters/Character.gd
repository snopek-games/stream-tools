extends KinematicBody2D

onready var sprite = $Sprite
onready	var name_label = $NameLabel
onready var speech_bubble = $SpeechBubble
onready var speech_timer = $SpeechTimer
onready var animation_player = $AnimationPlayer

var vector: Vector2
var speed: float = 100
var fade_out_on_speech: bool = true

var speech_queue := []

signal character_clicked (event)
signal character_faded_out ()

func _ready() -> void:
	speech_bubble.visible = false

	randomize_position_and_vector()
	sprite.randomize_character()

func randomize_position_and_vector():
	var viewport_size: Vector2 = get_viewport().get_visible_rect().size
	var sprite_size = sprite.get_size()

	global_position = Vector2(
		(sprite_size.x / 2) + randi() % int(viewport_size.x - sprite_size.x),
		(sprite_size.y / 2) + randi() % int(viewport_size.y - sprite_size.y))
	vector = Vector2(rand_range(-1.0, 1.0), rand_range(-1.0, 1.0)).normalized()

func set_name_label(name: String) -> void:
	name_label.text = name

func set_speech(text: String) -> void:
	speech_queue.clear()
	speech_timer.stop()
	add_speech(text)

func add_speech(text: String) -> void:
	speech_queue.append(text)
	if speech_timer.is_stopped():
		_show_next_speech_text()

func _show_next_speech_text() -> void:
	if speech_queue.size() == 0:
		speech_bubble.visible = false
		return

	if fade_out_on_speech:
		start_fading_out()

	var text = speech_queue.pop_front()
	speech_bubble.visible = true
	speech_bubble.set_speech_text(text)
	speech_timer.start()

func start_fading_out() -> void:
	animation_player.stop()
	animation_player.play("FadeOut")

func _on_SpeechTimer_timeout() -> void:
	_show_next_speech_text()

func _physics_process(delta: float) -> void:
	var viewport_size: Vector2 = get_viewport().get_visible_rect().size
	var sprite_size = sprite.get_size()

	var collision_data := move_and_collide(vector * speed * delta)
	if collision_data:
		vector = vector.bounce(collision_data.normal).normalized()
	elif position.x - (sprite_size.x / 2) < 0:
		vector = vector.bounce(Vector2.RIGHT).normalized()
	elif position.x + (sprite_size.x / 2) > viewport_size.x:
		vector = vector.bounce(Vector2.LEFT).normalized()
	elif position.y - (sprite_size.y / 2) < 0:
		vector = vector.bounce(Vector2.DOWN).normalized()
	elif position.y + (sprite_size.y / 2) > viewport_size.y:
		vector = vector.bounce(Vector2.UP).normalized()

func _on_Character_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton:
		emit_signal("character_clicked", event)

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "FadeOut":
		emit_signal("character_faded_out")
